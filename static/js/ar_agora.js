function initCanvas(){
// init WebAR.rock.mirror:
  WebARRocksMirror.init({
    isGlasses: false,

    //videoURL: '../../../../testVideos/1056010826-hd.mp4', // use a video from a file instead of camera video

    specWebARRocksFace: {
      NNCPath: nnPath,
      scanSettings: { // harden detection:
        threshold: 0.9,
        nDetectsPerLoop: 0,
        nScaleLevels: 3,
      },
      stabilizationSettings: {

      }
    },

    solvePnPObjPointsPositions: {
      "noseLeft": [21.862150,-0.121031,67.803383], // 1791
      "noseRight": [-20.539499,0.170727,69.944778], // 2198

      "leftEyeExt": [44.507431,34.942841,38.750019], // 1808
      "rightEyeExt": [-44.064968,35.399670,39.362930], // 2214

      "leftEarTop": [89.165428,16.312811,-49.064980], // 3870
      "leftEarBase": [78.738243,-6.044550,-23.177490], // 2994
      "leftEarBottom": [78.786850,-41.321789,-24.603769], // 1741

      "rightEarTop": [-88.488602,17.271400,-48.199409], // 5622
      "rightEarBase": [-78.156998,-5.305619,-22.164619], // 4779
      "rightEarBottom": [-78.945511,-41.255100,-26.536131], // 5641

      "leftTemple": [60.262970,83.790382,-13.540310], // 108
      "rightTemple": [-60.034760,83.584427,-13.248530], // 286

      "foreHead": [-1.057755,97.894547,24.654940], // 696
    },
    solvePnPImgPointsLabels: [
      "foreHead",
      "leftTemple", "rightTemple",
      "leftEarTop", "rightEarTop",
      "leftEyeExt", "rightEyeExt",
      "rightEarBottom", "leftEarBottom",
    ],

    canvasFace: canvasFace,
    canvasThree: canvasThree,

    // initial canvas dimensions:
    width: 1080,
    height: 720,

    // The occluder is a placeholder for the head. It is rendered with a transparent color
    // (only the depth buffer is updated).
    occluderURL: occluderPath,
    modelURL: modelPath, //initial model loaded. false or null -> no model
    envmapURL: envPath,

    // lighting:
    pointLightIntensity: 0.8, //intensity of the point light. Set to 0 to disable
    pointLightY: 200, // larger -> move the pointLight to the top
    hemiLightIntensity: 0, // intensity of the hemispheric light. Set to 0 to disable (not really useful if we use an envmap)

    // temporal anti aliasing - Number of samples. 0 -> disabled:
    taaLevel: 3,

    // debug flags - all should be false for production:
    debugLandmarks: false,
    debugOccluder: false
  });
}

function initClient() {
    const client = AgoraRTC.createClient({ mode: "live", role: "host", codec: "h264" });
    client.init(appId, function() {
        console.log("client initialized");
    }, function(err) {
        console.log("client init failed ", err);
    });

    client.join(token, "test", uid);

    return client;
}
//
function startAgora() {

  var stream = canvasComposite.captureStream(30);
  var tracks = stream.getVideoTracks()[0];

  client = initClient();
  const agoraStream = AgoraRTC.createStream({streamID: uid, audio: true, video: true, uid: uid, videoSource: tracks});

  agoraStream.init(function () {
    agoraStream.play("local_stream", {fit: "contain"});
    client.publish(agoraStream);
  });
}

let canvasFace, canvasThree, canvasComposite, compositeCtx, client;
let face, three;

function drawComposite() {
    compositeCtx.drawImage(canvasFace, 0, 0, canvasFace.width, canvasFace.height);
    compositeCtx.drawImage(canvasThree, 0, 0, canvasThree.width, canvasThree.height);
    compositeCtx.restore();
}



function main(){
  // get the 2 canvas from the DOM:
  canvasFace = document.getElementById('WebARRocksFaceCanvas');
  canvasThree = document.getElementById('threeCanvas');

  canvasComposite = document.getElementById('composite');

//  canvasFace.addEventListener('resise', (event) => {
//
//  });

  compositeCtx = canvasComposite.getContext("2d");
  initCanvas();

  setInterval(drawComposite, 1000 / 30);

  startAgora()
  console.log(canvasFace);
  canvasComposite.width = canvasFace.width
  canvasComposite.height = canvasFace.height


}


window.addEventListener('load', main);